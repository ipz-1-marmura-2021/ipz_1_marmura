namespace TCP
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Users
    {
        public int id { get; set; }

        [StringLength(101)]
        public string Login { get; set; }

        [StringLength(101)]
        public string Password { get; set; }
    }
}
