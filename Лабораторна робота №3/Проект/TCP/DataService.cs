﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace TCP
{
    class DataService
    {
        private readonly DataBase _dbContext;
        public DataService(DataBase dbContext)
        {
            _dbContext = dbContext;
        }



        public List<Users> GetAllUsers()
        {
            return _dbContext.Users.ToList();
        }


        public Users GetUser(string login, string password)
        {
            return _dbContext.Users.Where(el => el.Login.Equals(login) && el.Password.Equals(password)).FirstOrDefault();
        }



        public void InsertUser(Users user)
        {
            var userInDb = _dbContext.Users.FirstOrDefault(el => el.Login.Equals(user.Login));
            if (userInDb != null)
            {
                throw new ArgumentException("Login already exists in the database");
            }
            var query = @"INSERT INTO Users (Login_, Password_) VALUES (@Login, @Password)";
            _dbContext.Database.ExecuteSqlCommand(query, new SqlParameter("@Login", user.Login), new SqlParameter("@Password", user.Password));
            var userId = _dbContext.Users.FirstOrDefault(el => el.Login.Equals(user.Login)).id;
        }

        public List<string> SearchByAge(string Kids_age)
        {
            List<string> Chairs = _dbContext.Database.SqlQuery<string>("SELECT Name_Chair FROM Chair WHERE Kids_age like '%" + Kids_age + "%'").ToList();
            return Chairs;
        }

        public List<string> SearchByFirm(string Firm)
        {
            List<string> Chairs = _dbContext.Database.SqlQuery<string>("SELECT Name_Chair from Chair where Firm like '" + Firm + "%'").ToList();
            return Chairs;
        }

        public List<string> SearchByProducer(string Producer)
        {
            List<string> Chairs = _dbContext.Database.SqlQuery<string>("SELECT Name_Chair from Chair where Producer like '" + Producer + "%'").ToList();
            return Chairs;
        }


        public string GetOrders(string Chairs)
        {
            var query = @"INSERT INTO Orders (Name_Chair) VALUES (@Name_Chair)";
            _dbContext.Database.ExecuteSqlCommand(query, new SqlParameter("@Name_Chair", Chairs));

            string orders = _dbContext.Database.SqlQuery<string>("SELECT Name_Chair FROM Orders where Name_Chair = @Chair",
             new SqlParameter("@Chair", Chairs)).FirstOrDefault();
            return orders;
        }
        public void Order(string Chairs)
        {
            var query = @"INSERT INTO Orders (Name_Chair) VALUES (@Name_Chair)";
            _dbContext.Database.ExecuteSqlCommand(query, new SqlParameter("@Name_Chair", Chairs));
        }

    }
}