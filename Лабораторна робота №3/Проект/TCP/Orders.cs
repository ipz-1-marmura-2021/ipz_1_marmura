namespace TCP
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Orders
    {
        [Key]
        public int UserId { get; set; }

        [StringLength(25)]
        public string NameChair { get; set; }
    }
}
