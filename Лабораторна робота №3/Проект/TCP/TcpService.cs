﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using System.Reflection;


namespace TCP
{
    public class SocketNode
    {
        public string Method { get; set; }
        public string Args { get; set; }
    }

    public class TcpService
    {
        private readonly DataService dataService;
        public TcpService(DataBase context = null)
        {
            if (context != null)
                dataService = new DataService(context);
        }


        public string DecodeAndProcessRequest(string request)
        {
            var socketNode = JsonSerializer.Deserialize<SocketNode>(request);
            TcpMethods tcpMethod;
            string response = "";
            if (!Enum.TryParse<TcpMethods>(socketNode.Method, out tcpMethod))
            {
                tcpMethod = TcpMethods.NONE;
            }


            switch (tcpMethod)
            {
                case TcpMethods.Authorize:
                    response = Authorize(socketNode);
                    break;
                case TcpMethods.AddUser:
                    response = AddUser(socketNode);
                    break;
                case TcpMethods.SearchByAge:
                    response = SearchByAge(socketNode);
                    break;
                case TcpMethods.SearchByFirm:
                    response = SearchByFirm(socketNode);
                    break;
                case TcpMethods.SearchByProducer:
                    response = SearchByProducer(socketNode);
                    break;
                case TcpMethods.ShowOrderByAge:
                    response = ShowOrderByAge(socketNode);
                    break;
                case TcpMethods.ShowOrderByFirm:
                    response = ShowOrderByFirm(socketNode);
                    break;
                case TcpMethods.ShowOrderByProducer:
                    response = ShowOrderByProducer(socketNode);
                    break;
                case TcpMethods.Order:
                    response = Order(socketNode);
                    break;
                default:
                    break;
            }

            return response;
        }

        public async Task<string> DecodeStreamAsync(NetworkStream stream)
        {
            byte[] data = new byte[64];
            StringBuilder builder = new StringBuilder();
            int bytes = 0;
            do
            {
                bytes = await stream.ReadAsync(data, 0, data.Length);
                builder.Append(Encoding.UTF8.GetString(data, 0, bytes));
            }
            while (stream.DataAvailable);
            return builder.ToString();
        }

        public string DecodeStream(NetworkStream stream)
        {
            byte[] data = new byte[64];
            StringBuilder builder = new StringBuilder();
            int bytes = 0;
            do
            {
                bytes = stream.Read(data, 0, data.Length);
                builder.Append(Encoding.UTF8.GetString(data, 0, bytes));
            }
            while (stream.DataAvailable);
            return builder.ToString();
        }

        public async Task<byte[]> CodeStreamAsync(string request)
        {
            return await Task.Run(() => CodeStream(request));
        }

        public byte[] CodeStream(string request)
        {
            return Encoding.UTF8.GetBytes(request);
        }

        public string SerializeAuthorizeRequest(string login, string password)
        {
            return JsonSerializer.Serialize<SocketNode>(new SocketNode
            {
                Method = "Authorize",
                Args = JsonSerializer.Serialize<Users>(new Users
                {
                    Login = login,
                    Password = password
                })
            });
        }

        private string Authorize(SocketNode node)
        {
            string response = "";
            Users requestUser = JsonSerializer.Deserialize<Users>(node.Args);

            if (requestUser != null)
            {
                Users user = dataService.GetUser(requestUser.Login, requestUser.Password);
                if (user == null)
                {
                    user = new Users();
                }
                response = JsonSerializer.Serialize<Users>(user);
            }

            return response;
        }

        public Users DeserializeAuthorizeResponse(string response)
        {
            return JsonSerializer.Deserialize<Users>(response);
        }


        public string SerializeAddUserRequest(Users newUser)
        {
            return JsonSerializer.Serialize<SocketNode>(new SocketNode
            {
                Method = "AddUser",
                Args = JsonSerializer.Serialize<Users>(newUser)
            });
        }

        private string AddUser(SocketNode socketNode)
        {
            try
            {
                Users user = JsonSerializer.Deserialize<Users>(socketNode.Args);
                dataService.InsertUser(user);
                return "1";
            }
            catch (Exception ex)
            {
                return "0;" + ex.Message;
            }
        }

        public string SerializeSearchByAge(string Kind_age)
        {
            return JsonSerializer.Serialize<SocketNode>(new SocketNode
            {
                Method = "SearchByAge",
                Args = JsonSerializer.Serialize<string>(Kind_age)
            });
        }

        private string SearchByAge(SocketNode socketNode)
        {
            string response = "";
            string Kind_age = JsonSerializer.Deserialize<string>(socketNode.Args);
            List<string> Chairs = dataService.SearchByAge(Kind_age);
            response = JsonSerializer.Serialize<List<string>>(Chairs);
            return response;
        }

        public string SerializeSearchByFirm(string Firm)
        {
            return JsonSerializer.Serialize<SocketNode>(new SocketNode
            {
                Method = "SearchByFirm",
                Args = JsonSerializer.Serialize<string>(Firm)
            });
        }

        private string SearchByFirm(SocketNode socketNode)
        {
            string response = "";
            string Firm = JsonSerializer.Deserialize<string>(socketNode.Args);
            List<string> Chairs = dataService.SearchByFirm(Firm);
            response = JsonSerializer.Serialize<List<string>>(Chairs);
            return response;
        }

        public string SerializeSearchByProducer(string Producer)
        {
            return JsonSerializer.Serialize<SocketNode>(new SocketNode
            {
                Method = "SearchProducer",
                Args = JsonSerializer.Serialize<string>(Producer)
            });
        }

        private string SearchByProducer(SocketNode socketNode)
        {
            string response = "";
            string Producer = JsonSerializer.Deserialize<string>(socketNode.Args);
            List<string> Chairs = dataService.SearchByProducer(Producer);
            response = JsonSerializer.Serialize<List<string>>(Chairs);
            return response;
        }

        public List<string> DeserializeSearch(string response)
        {
            return JsonSerializer.Deserialize<List<string>>(response);
        }



        public string SerializeShowOrderByAge(string Chairs)
        {
            return JsonSerializer.Serialize<SocketNode>(new SocketNode
            {
                Method = "ShowOrderByAge",
                Args = JsonSerializer.Serialize<string>(Chairs)
            });
        }
        public string SerializeShowOrderByFirm(string Chairs)
        {
            return JsonSerializer.Serialize<SocketNode>(new SocketNode
            {
                Method = "ShowOrderByFirm",
                Args = JsonSerializer.Serialize<string>(Chairs)
            });
        }
        public string SerializeShowOrderByProducer(string Chairs)
        {
            return JsonSerializer.Serialize<SocketNode>(new SocketNode
            {
                Method = "ShowOrderByProducer",
                Args = JsonSerializer.Serialize<string>(Chairs)
            });
        }
        private string ShowOrderByAge(SocketNode socketNode)
        {
            string response = "";
            string Chairs = JsonSerializer.Deserialize<string>(socketNode.Args);
            List<string> Chair = dataService.SearchByAge(Chairs); dataService.SearchByFirm(Chairs); dataService.SearchByProducer(Chairs);
            Chair.Add(dataService.GetOrders(Chairs));
            response = JsonSerializer.Serialize<List<string>>(Chair);
            return response;
        }
        private string ShowOrderByFirm(SocketNode socketNode)
        {
            string response = "";
            string Chairs = JsonSerializer.Deserialize<string>(socketNode.Args);
            List<string> Chair = dataService.SearchByFirm(Chairs);
            Chair.Add(dataService.GetOrders(Chairs));
            response = JsonSerializer.Serialize<List<string>>(Chair);
            return response;
        }
        private string ShowOrderByProducer(SocketNode socketNode)
        {
            string response = "";
            string Chairs = JsonSerializer.Deserialize<string>(socketNode.Args);
            List<string> Chair = dataService.SearchByProducer(Chairs);
            Chair.Add(dataService.GetOrders(Chairs));
            response = JsonSerializer.Serialize<List<string>>(Chair);
            return response;
        }
        public List<string> DeserializeShowOrder(string response)
        {
            return JsonSerializer.Deserialize<List<string>>(response);
        }
        public string SerializeOrder(string Chairs)
        {
            return JsonSerializer.Serialize<SocketNode>(new SocketNode
            {
                Method = "Order",
                Args = JsonSerializer.Serialize<string>(Chairs)
            });
        }
        private string Order(SocketNode socketNode)
        {

            string Chairs = JsonSerializer.Deserialize<string>(socketNode.Args);
            dataService.Order(Chairs);
            return "Success";
        }
        public List<string> DeserializeOrder(string response)
        {
            return JsonSerializer.Deserialize<List<string>>(response);
        }

    }



    public enum TcpMethods
    {
        NONE,
        Authorize,
        AddUser,
        SearchByAge,
        SearchByFirm,
        SearchByProducer,
        ShowOrderByAge,
        ShowOrderByFirm,
        ShowOrderByProducer,
        Order
    }
}