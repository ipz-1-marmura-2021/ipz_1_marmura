namespace TCP
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Chair")]
    public partial class Chair
    {
        public int id { get; set; }

        [StringLength(25)]
        public string Name_Chair { get; set; }

        [StringLength(25)]
        public string Number_Chairs { get; set; }

        [StringLength(25)]
        public string Price_Chairs { get; set; }

        public int? Kids_age { get; set; }

        [StringLength(25)]
        public string Firm { get; set; }

        [StringLength(25)]
        public string Producer { get; set; }
    }
}
