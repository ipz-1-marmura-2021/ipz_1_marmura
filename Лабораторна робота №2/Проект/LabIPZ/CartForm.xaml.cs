﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.IO;
using TCP;

namespace LabIPZ
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class CartForm: Window
    {
        public CartForm()
        {
            InitializeComponent();
        }

        private void ButtonShutdown(object sender, RoutedEventArgs e) {

            Application.Current.Shutdown();
        }
        private void ButtonMinimize(object sender, RoutedEventArgs e)
        {

           
            
        }
        private void ButtonAccount(object sender, RoutedEventArgs e)
        {
            Hide();
            WelcomeForm log = new WelcomeForm();
            log.ShowDialog();
        }

        private void Grid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }
    }
}
