﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LabIPZ
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class WelcomeForm : Window
    {
        public WelcomeForm()
        {
            InitializeComponent();
        }

        private void ButtonShutdown(object sender, RoutedEventArgs e)
        {

            Application.Current.Shutdown();
        }
        private void ButtonMinimize(object sender, RoutedEventArgs e)
        {



        }
        private void ButtonAccount(object sender, RoutedEventArgs e)
        {
            Hide();
            WelcomeForm log = new WelcomeForm();
            log.ShowDialog();
        }


        private void Grid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Hide();
            MainWindow reg = new MainWindow();
            reg.ShowDialog();
        }

        private void Button_Log_Click(object sender, RoutedEventArgs e)
        {

            Hide();
            LoginForm One = new LoginForm();
            One.Show();
            this.Close();
        }

        private void Button_Reg_Click(object sender, RoutedEventArgs e)
        {
            Hide();
            RegistrationForm Two = new RegistrationForm();
            Two.Show();
            this.Close();
        }
    }
    }

