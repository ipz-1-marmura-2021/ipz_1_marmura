﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TCP;

namespace LabIPZ
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class RegistrationForm : Window
    {
        private readonly TcpService tcpService;
        public RegistrationForm()
        {
            InitializeComponent();
            tcpService = new TcpService();
        }

        private void ButtonShutdown(object sender, RoutedEventArgs e)
        {

            this.Close();
        }
        private void ButtonMinimize(object sender, RoutedEventArgs e)
        {



        }
        private void ButtonAccount(object sender, RoutedEventArgs e)
        {
            Hide();
            WelcomeForm log = new WelcomeForm();
            log.ShowDialog();
            this.Close();
        }
        private void Grid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Hide();
            WelcomeForm reg = new WelcomeForm();
            reg.ShowDialog();
        }


        private async void Button_Reg_Click(object sender, RoutedEventArgs e)
        {
            string login = textBoxLogin.Text;
            string password = passBox.Password;
            string pass_2 = passBox_2.Password;

            if (login.Length < 5)
            {
                textBoxLogin.ToolTip = "Login entered incorrectly";
                textBoxLogin.Background = Brushes.LightYellow;
            }
            else if (password.Length < 5)
            {
                passBox.ToolTip = "Password entered incorrectly";
                passBox.Background = Brushes.LightYellow;
            }

            if (passBox.Password != passBox_2.Password)
            {
                throw new ArgumentException("Passwords aren't the same");
            }

            var requestUser = new Users
            {
                Login = login,
                Password = password,
            };
            string request = tcpService.SerializeAddUserRequest(requestUser);
            byte[] data = await tcpService.CodeStreamAsync(request);
            await SingletoneObj.Stream.WriteAsync(data, 0, data.Length);
            string response = await tcpService.DecodeStreamAsync(SingletoneObj.Stream);
            var responseArgs = response.Split(';');
            if (responseArgs.Length > 1 && responseArgs[0].Contains("0"))
            {
                throw new ArgumentException(responseArgs[1]);
            }
            if (responseArgs.Contains("1"))
            {
                MessageBox.Show("All right");
            }
            MainWindow2 Three = new MainWindow2();
            Three.Show();
            this.Close();
           
        }

   
    }
    }

