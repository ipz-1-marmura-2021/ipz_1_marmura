﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net.Sockets;
using System.Reflection;
using System.Threading;
using TCP;


namespace LabIPZ
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class LoginForm : Window
    {
        public readonly TcpService tcpService;
        //public static Users user;
        public LoginForm()
        {
            InitializeComponent();
            this.tcpService = new TcpService();
        }

        private async void ButtonShutdown(object sender, RoutedEventArgs e)
        { 
            this.Close();
            string request = "LogOut";
            byte[] data = await tcpService.CodeStreamAsync(request);
            await SingletoneObj.Stream.WriteAsync(data, 0, data.Length);
        }
        private void ButtonMinimize(object sender, RoutedEventArgs e)
        {



        }
        private void ButtonAccount(object sender, RoutedEventArgs e)
        {
            Hide();
            WelcomeForm log = new WelcomeForm();
            log.ShowDialog();
        }


        private void Grid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Hide();
            WelcomeForm reg = new WelcomeForm();
            reg.ShowDialog();
        }

        private void Button_Log_Click(object sender, RoutedEventArgs e)
        {
            string login = textBoxLogin1.Text.Trim();
            string password = passBox1.Password.Trim();
            string request = tcpService.SerializeAuthorizeRequest(login, password);

            byte[] data = tcpService.CodeStream(request);
            SingletoneObj.Stream.Write(data, 0, data.Length);
            string response = tcpService.DecodeStream(SingletoneObj.Stream);
            Users user = tcpService.DeserializeAuthorizeResponse(response);

            if (login.Length < 5)
            {
                textBoxLogin1.ToolTip = "Login entered incorrectly";
                textBoxLogin1.Background = Brushes.LightYellow;
            }
            else if (password.Length < 5)
            {
                passBox1.ToolTip = "Password entered incorrectly";
                passBox1.Background = Brushes.LightYellow;
            }
            else
            {
                textBoxLogin1.ToolTip = "";
                textBoxLogin1.Background = Brushes.Transparent;
                passBox1.ToolTip = "";
                passBox1.Background = Brushes.Transparent;
                MessageBox.Show("All right");
                MainWindow2 Three = new MainWindow2();
                Three.Show();
                this.Close();
            }
            
        }


        private void textBoxLogin1_TextChanged(object sender, TextChangedEventArgs e)
        {

        }
    }
    }

