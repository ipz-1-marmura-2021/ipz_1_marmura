﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TCP;

namespace LabIPZ
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class Search : Window
    {
        public string curChairs;
        private readonly TcpService tcpService;
        public Search()
        {
            tcpService = new TcpService();
            InitializeComponent();
        }

        private void ButtonShutdown(object sender, RoutedEventArgs e)
        {

            this.Close();
        }
        private void ButtonMinimize(object sender, RoutedEventArgs e)
        {



        }
        private void ButtonAccount(object sender, RoutedEventArgs e)
        {
            Hide();
            WelcomeForm log = new WelcomeForm();
            log.ShowDialog();
        }


        private void Grid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private async void Button_Click(object sender, RoutedEventArgs e)
        {
			string request = " ";
			string name = textBoxName.Text;
			if (comboBox.SelectedIndex == -1)
			{
				throw new ArgumentException("Type of searching isn't selected");
			}
			if (comboBox.SelectedIndex == 0)
			{
				request = tcpService.SerializeSearchByAge(textBoxName.Text);
			}
			if (comboBox.SelectedIndex == 1)
			{
				request = tcpService.SerializeSearchByFirm(textBoxName.Text);
			}
			if (comboBox.SelectedIndex == 2)
			{
				request = tcpService.SerializeSearchByProducer(textBoxName.Text);
			}

			byte[] data = await tcpService.CodeStreamAsync(request);
			await SingletoneObj.Stream.WriteAsync(data, 0, data.Length);
			string response = await tcpService.DecodeStreamAsync(SingletoneObj.Stream);
			List<string> Chairs = tcpService.DeserializeSearch(response);
			listBox.ItemsSource = Chairs;
			if (Chairs.Count == 0 && comboBox.SelectedIndex == 0)
			{
				throw new ArgumentException("This Age isn't exist");
			}
			else if (Chairs.Count == 0 && comboBox.SelectedIndex == 1)
			{
				throw new ArgumentException("This Firm isn't exist");
			}
			else if (Chairs.Count == 0 && comboBox.SelectedIndex == 2)
			{
				throw new ArgumentException("This Producer isn't exist");
			}
			if (name.Length < 2)
			{
				textBoxName.ToolTip = "Nothing was entered";
				textBoxName.Background = Brushes.LightYellow;
			}
			else
			{
				textBoxName.ToolTip = "";
				textBoxName.Background = Brushes.Transparent;
			}
		}

        private async void Button_O_Click(object sender, RoutedEventArgs e)
        {
			string request = " ";
			if (listBox.SelectedIndex == -1)
			{
				throw new ArgumentException("Chair isn't selected");
			}
			request = tcpService.SerializeOrder(listBox.SelectedItem.ToString());
			byte[] data = await tcpService.CodeStreamAsync(request);
			await SingletoneObj.Stream.WriteAsync(data, 0, data.Length);
			string response = await tcpService.DecodeStreamAsync(SingletoneObj.Stream);
			//List<string> books = tcpService.DeserializeSearch(response);
			//curBooks = listBox.SelectedItem.ToString();
			//textBoxName.Text = "Selected book: " + listBox.SelectedItem;
			//textBoxName.Text = textBoxName.Text.Replace("System.Windows.Controls.ListBoxItem: ", "");
			MessageBox.Show("Order completed");
			CartForm Five = new CartForm();
			Five.Show();
			this.Close();
        }
		private void listBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{

		}
	}

}
