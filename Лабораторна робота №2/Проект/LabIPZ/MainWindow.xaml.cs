﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.IO;
using TCP;

namespace LabIPZ
{
    public static class SingletoneObj
    {
        public static Users User { get; set; }
        public static int Port { get; set; }
        public static string IP { get; set; }
        public static TcpClient Client { get; set; }
        public static NetworkStream Stream { get; set; }
    }
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public readonly TcpService tcpService;
        //public static Users user;
        public static string ip;
        public static int port;
        public MainWindow()
        {
            InitializeComponent();
            this.tcpService = new TcpService();
            SingletoneObj.Port = 8080;
            SingletoneObj.IP = "127.0.0.1";

            SingletoneObj.Client = new TcpClient(SingletoneObj.IP, SingletoneObj.Port);
            SingletoneObj.Stream = SingletoneObj.Client.GetStream();
        }

        private void ButtonShutdown(object sender, RoutedEventArgs e) {

            Application.Current.Shutdown();
        }
        private void ButtonMinimize(object sender, RoutedEventArgs e)
        {

           
            
        }
        private void ButtonAccount(object sender, RoutedEventArgs e)
        {
            Hide();
            WelcomeForm log = new WelcomeForm();
            log.Show();
            this.Close();
           
        }

        private void Grid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Увійдіть в аккаунт для покупки!");
        }
    }
}
